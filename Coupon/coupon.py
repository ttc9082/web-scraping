__author__ = 'TTc9082'
# -*- coding: utf-8 -*-
import urllib2
import urllib
import re
import thread
import time
import MySQLdb
from bs4 import BeautifulSoup


class DBConnection:
    def __init__(self):
        self.conn = MySQLdb.connect(host='localhost',user='root',
                               passwd='',db='CouponScraping')

    def InsertStore(self, store, link):
        cursor = self.conn.cursor()
        sql = 'insert into Store(store_name,store_href) values ("%s","%s")' % (store, link)
        try:
            cursor.execute(sql)
        except Exception,e:
            print e
        cursor.close()
        self.conn.commit()
        return 1

    def InsertCouponFromList(self, dictList, store_name):
        cursor = self.conn.cursor()
        for dict in dictList:
            if dict['vote'] != None:
                sql = 'insert into Coupon(store_name,isExpired,`TYPE`,title,vote) values ("%s","%d","%s","%s","%d")' % (store_name, dict['exp'], dict['type'],dict['name'], dict['vote'])
                try:
                    cursor.execute(sql)
                except Exception,e:
                    print e
                    try:
                        print 'try to insert with error title.'
                        sql = 'insert into Coupon(store_name,isExpired,`TYPE`,title,vote) values ("%s","%d","%s","%s","%d")' % (store_name, dict['exp'], dict['type'],'error,cant show.', dict['vote'])
                        cursor.execute(sql)
                    except Exception,e:
                        print e
            else:
                sql = 'insert into Coupon(store_name,isExpired,`TYPE`,title) values ("%s","%d","%s","%s")' % (store_name, dict['exp'], dict['type'],dict['name'])
                try:
                    cursor.execute(sql)
                except Exception,e:
                    print e
                    try:
                        print 'try to insert with error title.'
                        sql = 'insert into Coupon(store_name,isExpired,`TYPE`,title) values ("%s","%d","%s","%s")' % (store_name, dict['exp'], dict['type'],'error,cant show.')
                        cursor.execute(sql)
                    except Exception,e:
                        print e
        cursor.close()
        self.conn.commit()
        return 1

    def InsertStoreAndCategoryAndTheirRelation(self,dict):
            print 'insert store'
            cursor = self.conn.cursor()
            sql = 'insert into Store(store_name,store_href) values ("%s","%s")'% (dict['store'], dict['link'])
            try:
                cursor.execute(sql)
            except Exception,e:
                print e
            cursor.close()
            self.conn.commit()
            print 'search store id'
            cursor = self.conn.cursor()
            sql = 'select sid from Store where store_name = "%s"' % (dict['store'])
            try:
                cursor.execute(sql)
            except Exception,e:
                print e
            sid = cursor.fetchall()[0][0]
            print sid
            cursor.close()
            self.conn.commit()
            for cate in dict['cate']:
                print 'try get cate', cate
                cursor = self.conn.cursor()
                sql = 'select cid from Category where category_name = "%s"'% (cate)
                try:
                    cursor.execute(sql)
                except Exception,e:
                    print e
                cate_id = cursor.fetchall()
                cursor.close()
                self.conn.commit()
                print cate_id
                if cate_id == ():
                    print 'create new cate'
                    cursor = self.conn.cursor()
                    sql = 'insert into Category(category_name) values ("%s")'% (cate)
                    try:
                        cursor.execute(sql)
                    except Exception,e:
                        print e
                    cursor.close()
                    self.conn.commit()
                print 'get cate_id'
                cursor = self.conn.cursor()
                sql = 'select cid from Category where category_name = "%s"'% (cate)
                try:
                    cursor.execute(sql)
                except Exception,e:
                    print e
                cate_id = cursor.fetchall()[0][0]
                cursor.close()
                self.conn.commit()
                print cate_id
                cursor = self.conn.cursor()
                sql = 'insert into c_s(cid,sid) values ("%s","%s")'% (cate_id, sid)
                try:
                    cursor.execute(sql)
                except Exception,e:
                    print e
                cursor.close()
                self.conn.commit()
            return 1


def GetStores():
    items = []
    for c in C_list:
        myUrl = "http://www.coupons.com/coupon-codes/stores/" + c
        try:
            print 'starting with ', c
            myResponse  = urllib2.urlopen(myUrl)
            myPage = myResponse.read()
            soup = BeautifulSoup(myPage)
            storeListRaw = soup.find_all('li',{'class': 'item'})
            for store in storeListRaw:
                store_info =[store.a['href'], store.a.text]
                print store_info
                if '&' not in store.a.text:
                    items.append(store_info)
        except:
            time.sleep(20)
    return items


def GetCouponsAndCategoriesAndRelationshipsFromStores(StoresNameList):
    Base_url = 'http://www.coupons.com'
    for item in StoresNameList:
        couponDictList = []
        link = item[0]
        myUrl = Base_url + link
        print myUrl
        myResponse  = urllib2.urlopen(myUrl)
        myPage = myResponse.read()
        soup = BeautifulSoup(myPage)
        # find coupons and save in db
        couponListRaw = soup.find_all('div', {'class': 'ccpod'})
        for couponRaw in couponListRaw:
            couponName = couponRaw.h3.text
            couponType = ' '.join(couponRaw['class'][1:-1])
            couponUsed = None
            try:
                couponUsed = int(couponRaw.find('div',{'class': 'clicks'}).text.replace(" ", '')[6:-7])
            except:
                pass
            couponDict = {'name': couponName, 'type': couponType, 'exp': 0, 'vote': couponUsed}
            couponDictList.append(couponDict)
        dbc.InsertCouponFromList(couponDictList, item[1])
        # find categories and save in db
        categoryListRaw = soup.find_all('div', {'class': 'breadcrumbs'})[0].find_all('a')
        print categoryListRaw
        if categoryListRaw != []:
            cates = []
            for category in categoryListRaw:
                cates.append(category.text)
            print cates
            dict = {}
            dict['cate'] = cates
            dict['store'] = item[1]
            dict['link'] = item[0]
            print dict
            dbc.InsertStoreAndCategoryAndTheirRelation(dict)
        else:
            dbc.InsertStore(item[1], item[0]) # when they don't, only save store in db, no category or relation will be added in the db


C_list = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0-9']
dbc = DBConnection()
store_list = GetStores()
GetCouponsAndCategoriesAndRelationshipsFromStores(store_list)
