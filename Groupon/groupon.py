__author__ = 'TTc9082'
# -*- coding: utf-8 -*-
import urllib2
import urllib
import re
import thread
import time
import MySQLdb
from bs4 import BeautifulSoup
import threading
import random
import time


class DBConnection:
    def __init__(self):
        self.conn = MySQLdb.connect(host='localhost',user='root',
                               passwd='',db='GrouponScraping')

    def InsertStore(self, store,link):
        cursor = self.conn.cursor()
        sql = 'insert into Store(store_name,store_href) values ("%s","%s")' % (store,link)
        try:
            cursor.execute(sql)
        except Exception,e:
            print e
        cursor.close()
        self.conn.commit()
        return 1

    def InsertStoreFromList(self, list):
        cursor = self.conn.cursor()
        for item in list:
            link = item[0]
            name = item[1]
            sql = 'insert into Store(store_name,store_href) values ("%s","%s")' % (name,link)
            try:
                cursor.execute(sql)
            except Exception,e:
                print e
        cursor.close()
        self.conn.commit()
        return 1

    def InsertCouponFromList(self, dictList, store_name):
        for dict in dictList:
            cursor = self.conn.cursor()
            sql = "select cou_id from Coupon where title = '%s' and store_name='%s'" % (dict['name'], store_name)
            try:
                cursor.execute(sql)
            except Exception, e:
                print e
            self.conn.commit()
            cou = cursor.fetchall()
            if not cou:
                cursor = self.conn.cursor()
                sql = 'insert into Coupon(store_name,isExpired,`TYPE`,title,isExclusive) values ("%s","%d","%s","%s","%d")' % (store_name, dict['exp'], dict['type'],dict['name'],dict['exc'])
                try:
                    cursor.execute(sql)
                    cou_id = cursor.lastrowid
                except Exception, e:
                    print e
                    try:
                        print 'try to insert with error title.'
                        sql = 'insert into Coupon(store_name,isExpired,`TYPE`,title,isExclusive) values ("%s","%d","%s","%s","%d")' % (store_name, dict['exp'], dict['type'],'error,cant show.',dict['exc'])
                        cursor.execute(sql)
                        cou_id = cursor.lastrowid
                    except Exception, e:
                        print e
            else:
                cou_id = cou[0][0]
            cursor.close()
            self.conn.commit()
            return cou_id

    def InsertCategoryFromList(self, List, store_name):
        cursor = self.conn.cursor()
        for cata in List:
            sql = 'insert into Category(store_name,isExpired,`TYPE`,title,isExclusive) values ("%s","%d","%s","%s","%d")' % (store_name, dict['exp'], dict['type'],dict['name'],dict['exc'])
            try:
                cursor.execute(sql)
            except Exception,e:
                print e
        cursor.close()
        self.conn.commit()
        return 1

    def InsertStoreAndCategoryAndTheirRelation(self,dict):
        print 'insert store'
        cursor = self.conn.cursor()
        sql = 'insert into Store(store_name,store_href) values ("%s","%s")'% (dict['store'], dict['link'])
        try:
            cursor.execute(sql)
        except Exception, e:
            print e
        cursor.close()
        self.conn.commit()
        print 'search store id'
        cursor = self.conn.cursor()
        sql = 'select sid from Store where store_name = "%s"' % (dict['store'])
        try:
            cursor.execute(sql)
        except Exception,e:
            print e
        sid = cursor.fetchall()[0][0]
        print sid
        cursor.close()
        self.conn.commit()
        for cate in dict['cate']:
            print 'try get cate', cate
            cursor = self.conn.cursor()
            sql = 'select cid from Category where category_name = "%s"'% (cate)
            try:
                cursor.execute(sql)
            except Exception,e:
                print e
            cate_id = cursor.fetchall()
            cursor.close()
            self.conn.commit()
            print cate_id
            if cate_id == ():
                print 'create new cate'
                cursor = self.conn.cursor()
                sql = 'insert into Category(category_name) values ("%s")'% (cate)
                try:
                    cursor.execute(sql)
                except Exception,e:
                    print e
                cursor.close()
                self.conn.commit()
            print 'get cate_id'
            cursor = self.conn.cursor()
            sql = 'select cid from Category where category_name = "%s"'% (cate)
            try:
                cursor.execute(sql)
            except Exception,e:
                print e
            cate_id = cursor.fetchall()[0][0]
            cursor.close()
            self.conn.commit()
            print cate_id
            cursor = self.conn.cursor()
            sql = 'insert into c_s(cid,sid) values ("%s","%s")'% (cate_id, sid)
            try:
                cursor.execute(sql)
            except Exception,e:
                print e
            cursor.close()
            self.conn.commit()
        return 1

    def get_or_insert_category(self, category_name, category_url):
        cursor = self.conn.cursor()
        sql = "select cid from Category where category_name = '%s'" % category_name
        try:
            cursor.execute(sql)
        except Exception, e:
            print e
        cursor.close()
        self.conn.commit()
        c = cursor.fetchall()
        if not cursor.fetchall():
            cursor = self.conn.cursor()
            sql = "insert into Category(`category_name`, `category_url`) value ('%s', '%s')" % (category_name, category_url)
            cursor.execute(sql)
            cid = cursor.lastrowid
            cursor.close()
            self.conn.commit()
        else:
            cid = c[0][0]
        return cid

    def insert_c_cou(self, cid, cou_id):
        cursor = self.conn.cursor()
        sql = "insert into c_cou(`cid`, `cou_id`) value ('%s', '%s')" % (cid, cou_id)
        cursor.execute(sql)
        id = cursor.lastrowid
        cursor.close()
        self.conn.commit()
        return id


def GetStoreName(C_list):
    aitems = []
    for c in C_list:
        myUrl = "http://www.groupon.com/coupons/stores/starting-with/"+ c
        try:
            myResponse  = urllib2.urlopen(myUrl)
            myPage = myResponse.read()
            unicodePage = myPage.decode("utf-8")
            soup = BeautifulSoup(myPage)
            items = soup.find_all('li', {'class':'columns three end'})
            for item in items:
                print item.find('a').text.strip().encode('ascii', 'ignore')
                aitems.append([item.find('a')['href'],item.find('a').text.strip().encode('ascii', 'ignore')])
            print 'Get stores whose name start with', c
        except:
            time.sleep(20)
    # dbc.InsertStoreFromList(items)
    return aitems


def GetCouponFromStore(StoresNameList,dbc):
    Base_url = 'http://www.groupon.com/'
    for item in StoresNameList:
        couponDictList = []
        link = item[0]
        myUrl = Base_url + link
        print myUrl
        # try:
        myResponse  = urllib2.urlopen(myUrl)
        myPage = myResponse.read()
        soup = BeautifulSoup(myPage)
        couponListRaw = soup.find_all('div',{'class':'title-inner'})
        for couponRaw in couponListRaw:
            couponName = couponRaw.h4.text.strip().replace("'",'').replace('"','').encode('ascii', 'ignore')
            couponType = couponRaw.find_all('span')[0].text
            if couponRaw.find_all('span').__len__() > 1:
                for Raw in couponRaw.find_all('span'):
                    if Raw.text == 'Groupon Exclusive':
                        couponExc = 1
                        break
                    else:
                        couponExc = 0
            else:
                couponExc = 0
            couponDict = {'name':couponName, 'type':couponType, 'exp':0, 'exc':couponExc}
            couponDictList.append(couponDict)
        dbc.InsertCouponFromList(couponDictList, item[1])
        # except:
        #     print 'Network error. Skip and wait 20s to start next.'
        #     time.sleep(20)


def GetCategoryFromStore(StoresNameList, dbc):
    Base_url = 'http://www.groupon.com/'
    for item in StoresNameList:
        link = item[0]
        myUrl = Base_url + link
        print myUrl
        myResponse  = urllib2.urlopen(myUrl)
        myPage = myResponse.read()
        soup = BeautifulSoup(myPage)
        categoryListRaw = soup.find_all('div',{'data-bhw':'RelatedCategories'})
        print categoryListRaw
        if categoryListRaw != []:
            categoryListRaw = categoryListRaw[0].find_all('a')
            cates = []
            for category in categoryListRaw:
                cates.append(category.text)
            print cates
            dict = {}
            dict['cate'] = cates
            dict['store'] = item[1]
            dict['link'] = item[0]
            print dict
            dbc.InsertStoreAndCategoryAndTheirRelation(dict)
        else:
            dbc.InsertStore(item[1],item[0])


def GetCouponFromCategory(category_dict, dbc):
    """
    category_dict Example: {'apparel' : '/coupons/categories/apparel'}
    """
    base_url = 'http://www.groupon.com'
    category_name, category_url = category_dict.popitem()
    cid = dbc.get_or_insert_category(category_name, category_url)
    url = base_url + category_url
    try:
        myResponse  = urllib2.urlopen(url, timeout=10)
    except:
        print 'try one more time',url
        myResponse  = urllib2.urlopen(url, timeout=10)
    myPage = myResponse.read()
    soup = BeautifulSoup(myPage)
    has_subcategory = soup.find('div', {'data-bhw': 'OtherSubcategories'})
    if has_subcategory:
        subcategories = has_subcategory.find_all('a')
        for subcategory in subcategories:
            subcategory_dict = {subcategory.text.encode('ascii', 'ignore').replace("'", "\\\'"): subcategory['href']}
            GetCouponFromCategory(subcategory_dict, dbc)
    total = soup.find('span', {'class': 'total'})
    if total:
        total_num = int(total.text)
        pages = int(total_num/50)
    else:
        pages = 0
    for i in xrange(pages+1):
        page_url = '?page=' + str(i)
        myResponse  = urllib2.urlopen(url+page_url)
        myPage = myResponse.read()
        soup = BeautifulSoup(myPage)
        coupons = soup.find_all('div', {'class': 'coupon'})
        for coupon in coupons:
            image_container = coupon.find('div', {'class': 'image-contain'})
            brand = image_container.div.a.img['alt'].encode('ascii', 'ignore').replace("'", "\\\'").replace('coupons', '')
            type = coupon.find('span', {'class': 'section-subtitle'}).text
            title = coupon.h4.text.replace('"', '').encode('ascii', 'ignore').replace("'", "\\\'")
            isExclusive = 1 if coupon.find('a', {'class': 'exclusive-badge'}) else 0
            cou_id = dbc.InsertCouponFromList([{'exp': 0, 'exc': isExclusive, 'name': title, 'type': type}], brand)
            dbc.insert_c_cou(cid, cou_id)


def get_all_categories():
    category_list = []
    url = 'http://www.groupon.com/coupons/categories'
    myResponse  = urllib2.urlopen(url)
    myPage = myResponse.read()
    soup = BeautifulSoup(myPage)
    categories = soup.find_all('li', {'class': 'columns three end'})
    for category in categories:
        category_list.append({category.text.encode('ascii', 'ignore').replace("'", "\\\'"): category.a['href']})
    return category_list


class groupon(threading.Thread):
    def __init__(self, char):
        self.dbc = DBConnection()
        threading.Thread.__init__(self)
        self.dbc = DBConnection()
        self.c_list = [char]

    def run(self):
        print "start thread", self.dbc
        self.store_list = GetStoreName(self.c_list)
        GetCouponFromStore(self.store_list,self.dbc)
        print "\n"

class category(threading.Thread):
    def __init__(self, category_dict):
        self.dbc = DBConnection()
        threading.Thread.__init__(self)
        self.category_dict = category_dict

    def run(self):
        print "start thread", self.dbc
        GetCouponFromCategory(self.category_dict, self.dbc)
        print "\n"

def main():
    threads = []
    for category_dict in category_list:
        thread = category(category_dict)
        threads.append(thread)
        thread.start()
        print len(threading.enumerate())
        while True:
            if(len(threading.enumerate()) < 30):
                break
            else:
                print '------wait------'
                time.sleep(10)
    for thread in threads:
        thread.join()
    print threads

category_list = get_all_categories()
main()