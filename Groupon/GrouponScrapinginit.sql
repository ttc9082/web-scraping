# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.10)
# Database: GrouponScraping
# Generation Time: 2014-04-28 19:28:46 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table c_cou
# ------------------------------------------------------------

DROP TABLE IF EXISTS `c_cou`;

CREATE TABLE `c_cou` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(11) unsigned DEFAULT NULL,
  `cou_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `c_c` (`cid`),
  KEY `s_s` (`cou_id`),
  CONSTRAINT `c_cou_ibfk_1` FOREIGN KEY (`cou_id`) REFERENCES `Coupon` (`cou_id`),
  CONSTRAINT `c_c` FOREIGN KEY (`cid`) REFERENCES `Category` (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Category`;

CREATE TABLE `Category` (
  `cid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) DEFAULT NULL,
  `category_url` varchar(100) DEFAULT '',
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Coupon
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Coupon`;

CREATE TABLE `Coupon` (
  `cou_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `store_name` varchar(100) DEFAULT NULL,
  `isExpired` tinyint(4) DEFAULT NULL,
  `isExclusive` tinyint(4) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `success_rate` int(11) DEFAULT NULL,
  `vote` int(110) DEFAULT NULL,
  `title` text,
  `detail` varchar(500) DEFAULT NULL,
  `comment_num` int(11) DEFAULT NULL,
  `people_used_today` int(11) DEFAULT NULL,
  PRIMARY KEY (`cou_id`),
  FULLTEXT KEY `coupon_title` (`title`),
  FULLTEXT KEY `coupon_i` (`title`),
  FULLTEXT KEY `coupon_haha` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
