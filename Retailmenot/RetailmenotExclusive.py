__author__ = 'TTc9082'
import urllib2
import MySQLdb
import random
import socket
import struct
from bs4 import BeautifulSoup
import json


class DBConnection:
    def __init__(self):
        self.conn = MySQLdb.connect(host='localhost', user='root', passwd='', db='rtmnex')

    def InserRetailmenotCoupon(self, dict):
        cursor = self.conn.cursor()
        sql = 'insert into Coupon(store_name,isExpired,`type`,title,vote,success_rate,comment_num,detail) values ("%s","%d","%s","%s","%d","%d","%s","%s")' % (
            dict['store'], dict['exp'], dict['type'], dict['name'], dict['vote'], dict['rate'], dict['cn'],
            dict['des'])
        try:
            cursor.execute(sql)
        except Exception, e:
            print e
            try:
                print 'try to insert with error title.'
                sql = 'insert into Coupon(store_name,isExpired,`type`,title,vote,success_rate,comment_num,detail) values ("%s","%d","%s","%s","%d","%d","%s","%s")' % (
                    dict['store'], dict['exp'], dict['type'], 'bad Char', dict['vote'], dict['rate'], dict['cn'],
                     'bad Char')
                cursor.execute(sql)
            except Exception, e:
                print e
        cursor.close()
        self.conn.commit()
        return 1

def get_exclusive():
    for i in range(1, 2):
        while True:
            try:
                print i
                IP = socket.inet_ntoa(struct.pack('>I', random.randint(1, 0xffffffff)))
                headers = {
                    'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6',
                    'Referer': 'http://www.google.com/',
                    'X-Forwarded-For': IP,
                    'cookie': 'country_code=US; out_originalReferrer=https://www.google.com/; out_originalLanding=http://www.retailmenot.com/; s_v16=%5B%5B%27www.google.com%27%2C%271395353848391%27%5D%2C%5B%27www.google.com%27%2C%271395353857893%27%5D%2C%5B%27www.google.com%27%2C%271395353857905%27%5D%5D; out_referrer=https://www.google.com/; dma_code=501; quick_feedback_count=2; f_out=1395955336835; s_dslc=1395955338777; user[userId]=3810749; user[sessionId]=781417e7-1828-4f53-a9f1-fde1b204f651; user[preferredUsername]=tt1068; user[uuid]=K6T32TWNNBAZLDRTOJFNQXLXUA; ss_t=search; og=; ov=control; nps_survey_pv=1; rmn-fc=W8LtMsMCcZ533490682d0ae533499f07b9fd; s_vnum=23; s_invisit=1; s_nr=1396020984578-repeat; rqid=W8LtMsMCcZ533596f4e1cb353359a0435859; slice=control; __utma=253539722.338546051.1395353847.1395953769.1396020982.26; __utmb=253539722.7.9.1396020984590; __utmc=253539722; __utmz=253539722.1395353847.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); out_referrerLanding=http://www.retailmenot.com/; s_vstart=1; s_cc=true; s_pre_pn=%2F; s_camp=no_campaign; s_dslv=1396021765936; s_sq=%5B%5BB%5D%5D; s_vi=[CS]v1|2995B47C05162663-40000182801137FA[CE]; userFlashVersion=W8LtMsMCcZ; visitor=58JwM6ehrL; session=W8LtMsMCcZ533596f4e1cb3; s_ppv=-%2C13%2C13%2C605'
                }
                myUrl = "http://www.retailmenot.com/search.php?json=true&query=exclusives&types[]=printable&page="+ str(i) +"&knifeset=&slice=control&csplit=control&vsplit=control&cread=control"
                print myUrl
                myRequest = urllib2.Request(myUrl, headers=headers)
                myResponse = urllib2.urlopen(myRequest)
                myPage = myResponse.read()
                coupon_dict = json.loads(myPage)
                soup = BeautifulSoup(coupon_dict['offers'])
                couponli = soup.find_all('li')
                for coupon in couponli:
                    while True:
                        try:
                            print coupon['data-offerid']
                            data_id = coupon['data-offerid']
                            c_url = "http://www.retailmenot.com/exclusives?c=" + data_id
                            myRequest = urllib2.Request(c_url, headers=headers)
                            myResponse = urllib2.urlopen(myRequest)
                            myPage = myResponse.read()
                            soup = BeautifulSoup(myPage)
                            coupon_detail = soup.find('div', {'class': 'slide-wrapper'}).find('li')
                            store_name = coupon_detail['data-storename']
                            title = coupon_detail.find('h3').text.strip().encode('ascii', 'ignore').replace('"', "")
                            type = coupon_detail.find('div', {'class': 'type_icon'})
                            if 'Sale' in type:
                                type = 'sale'
                            else:
                                type = 'code'
                            detail = coupon_detail.find('p', {'class': "discount"}).text.strip().encode('ascii', 'ignore').replace('"', "")
                            try:
                                comment = int(coupon_detail.find('span', {'class': 'commentBubble'}).text.replace(' ', ''))
                            except:
                                comment = 0
                            try:
                                success = int(coupon_detail.find('div', {'class': 'percent'}).text[0:-1])
                            except:
                                success = 99999
                            vote = int(coupon_detail.find('div', {'class': "vote_count"}).text.split(' ')[0])
                            dict_coupon = {'store': store_name, "des": detail,
                                           'name': title, 'rate': success,
                                           'vote': vote, 'exp': 0, 'cn': comment, 'type': type}
                            print dict_coupon
                            dbc.InserRetailmenotCoupon(dict_coupon)
                            break
                        except:
                            pass
                break
            except:
                pass
    print 'finished'
    return 0

dbc = DBConnection()
a = ['code','freeShipping','sale','printable'] # get exclusive from 4 categories, replace in the link first. and manully change the range by checking the website.
get_exclusive()