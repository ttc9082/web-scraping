__author__ = 'TTc9082'
import urllib2
import MySQLdb
import random
import socket
import struct
from bs4 import BeautifulSoup


class DBConnection:
    def __init__(self):
        self.conn = MySQLdb.connect(host='localhost', user='root', passwd='', db='Retailmenot')

    def insert_retailmenot_categories(self, category_list): # insert from list like ['electronics', '/coupons/electronics']
        categories_to_be_removed = [] # initialize a list for delete
        for cate in category_list:
            cursor = self.conn.cursor()
            sql = 'select cid from Category where category_name = "%s"' % cate[0]
            try:
                cursor.execute(sql)
            except Exception, e:
                print e
            cate_id = cursor.fetchall()
            cursor.close()
            self.conn.commit()
            if cate_id == ():
                print 'creating category:', cate[0]
                cursor = self.conn.cursor()
                sql = 'insert into Category(category_name, category_href) values ("%s","%s")' % (cate[0], cate[1])
                try:
                    cursor.execute(sql)
                except Exception, e:
                    print e
                cursor.close()
                self.conn.commit()
            else:
                # print cate[0], ' is already in DB'
                categories_to_be_removed.append(cate)
        # print 'removing ',len(categories_to_be_removed),' cates:' ,categories_to_be_removed
        for cate in categories_to_be_removed:
            if cate in category_list:
                category_list.remove(cate)
        return category_list

    def fuckthestoresandcates(self, store_list, cate):
        for store in store_list:
            # print 'name', store['store']
            cursor = self.conn.cursor()
            sql = 'select sid from Store where store_name = "%s"' % (store['store'])
            try:
                cursor.execute(sql)
            except Exception, e:
                print e
            sid = cursor.fetchall()
            print sid
            cursor.close()
            self.conn.commit()
            if sid == ():
                print 'create new store of ', store['store']
                cursor = self.conn.cursor()
                sql = 'insert into Store(store_name,store_href) values ("%s","%s")' % (store['store'], store['link'])
                try:
                    cursor.execute(sql)
                except Exception, e:
                    print e
                cursor.close()
                self.conn.commit()
            # print 'search store id of ', store['store']
            cursor = self.conn.cursor()
            sql = 'select sid from Store where store_name = "%s"' % (store['store'])
            try:
                cursor.execute(sql)
            except Exception, e:
                print e
            sid = cursor.fetchall()[0][0]
            # print 'sid:', sid
            cursor.close()
            self.conn.commit()
            cursor = self.conn.cursor()
            sql = 'select cid from Category where category_name = "%s"' % (cate[0])
            try:
                cursor.execute(sql)
            except Exception, e:
                print e
            cid = cursor.fetchall()[0][0]
            # print 'cid:', cid
            cursor.close()
            self.conn.commit()
            cursor = self.conn.cursor()
            sql = 'insert into c_s(cid,sid) values ("%s","%s")' % (cid, sid)
            try:
                cursor.execute(sql)
            except Exception, e:
                print e
            cursor.close()
            self.conn.commit()

    def getStoreURL(self, sid):
        cursor = self.conn.cursor()
        sql = 'select store_href from Store where sid = "%s"' % (sid)
        try:
            cursor.execute(sql)
        except Exception, e:
            print e
        url = cursor.fetchall()
        cursor.close()
        self.conn.commit()
        if url == ():
            return None
        else:
            print 'find:', url[0][0]
            return url[0][0]

    def InserRetailmenotCoupon(self, dictList):
        cursor = self.conn.cursor()
        for dict in dictList:
            sql = 'insert into Coupon(store_name,isExpired,`type`,title,vote,success_rate,comment_num,people_used_today,detail) values ("%s","%d","%s","%s","%d","%d","%s","%d","%s")' % (
                dict['store'], dict['exp'], dict['type'], dict['name'], dict['vote'], dict['rate'], dict['cn'],
                dict['use'],
                dict['des'])
            try:
                cursor.execute(sql)
            except Exception, e:
                print e
                try:
                    print 'try to insert with error title.'
                    sql = 'insert into Coupon(store_name,isExpired,`type`,title,vote,success_rate,comment_num,people_used_today,detail) values ("%s","%d","%s","%s","%d","%d","%s","%d","%s")' % (
                        dict['store'], dict['exp'], dict['type'], 'bad Char', dict['vote'], dict['rate'], dict['cn'],
                        dict['use'], 'bad Char')
                    cursor.execute(sql)
                except Exception, e:
                    print e
        cursor.close()
        self.conn.commit()
        return 1


def GetAllCouponFromStore(sid):
    print sid
    url = dbc.getStoreURL(sid)
    if url is None:
        print 'No url'
        raise
    else:
        IP = socket.inet_ntoa(struct.pack('>I', random.randint(1, 0xffffffff)))
        headers = {
            'User-Agent': "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) "
                          "Gecko/20091201 Firefox/3.5.6",
            'Referer': 'http://www.google.com/',
            'X-Forwarded-For': IP
        }
        myUrl = "http://www.retailmenot.com" + url
        print myUrl
        myRequest = urllib2.Request(myUrl, headers=headers)
        try:
            myResponse = urllib2.urlopen(myRequest)
        except:
            print '403'
            raise
        myPage = myResponse.read()
        soup = BeautifulSoup(myPage)
        pop = soup.find('div', {'class': 'major'}).find_all('div', {"class": 'offer'})
        if not soup.find_all('div', {'class': 'rmn-logo'}):
            print 'human check'
            raise
        # print ex.find_all('div', {'class': 'expired-offer'})[0].h3
        # print ex.find_all('div', {'class': 'expired-offer'})[0].find('span',{'class': 'js-vote-count'})
        # print ex.find_all('div', {'class': 'expired-offer'})[0].find('li',{'class': 'type-box'})
        # print ex.find_all('div', {'class': 'expired-offer'})[2].find('span', {'class': 'js-comment-count'})
        # for e in ex:
        #     print 'haha', e.h3.text, e.find('span',{'class': 'js-vote-count'}).text, e.find('li',{'class': 'type-box'}).text, e.find('span', {'class': 'js-comment-count'})
        couponList = []
        for p in pop:
            # print 'start', p.find('h3').text, p.find('p').span.text, p.find('li', {'class': 'type-box'}).text, p.find('span',{'class':'js-percent'}), p.find('span',{'class':'comm-num'})
            title = p.find('h3').text.encode("ascii", "ignore").replace("\"", "")
            vote = int(p.find('p').span.text)
            type = p.find('li', {'class': 'type-box'}).text
            if p.find('span', {'class': 'js-percent'}):
                rate = int(p.find('span', {'class': 'js-percent'}).text)
            else:
                rate = 99999
            try:
                cn = int(p.find('span', {'class': 'comm-num'}).text.replace(' ', ''))
            except:
                cn = 99999
            if 'expired-offer' in p['class']:
                exp = 1
            else:
                exp = 0
            if p.find('p', {'class': 'use-data'}):
                if p.find('p', {'class': 'use-data'}).text.split(' ')[1] == u'Last':
                    use = 99999
                else:
                    use = int(p.find('p', {'class': 'use-data'}).text.split(' ')[1])
            else:
                use = 99999
            des = p.find('p', {'class': 'description'}).text.encode("ascii", "ignore").replace("\"", "")
            couponList.append(
                {'store': sid, 'exp': exp, 'type': type, 'vote': vote, 'name': title, 'cn': cn, 'des': des,
                 'rate': rate, "use": use})
        dbc.InserRetailmenotCoupon(couponList)


def GetAllCategories(): # function that gets all main categories.
    IP = socket.inet_ntoa(struct.pack('>I', random.randint(1, 0xffffffff)))
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6',
        'Referer': 'http://www.google.com/',
        'X-Forwarded-For': IP
    }
    myUrl = "http://www.retailmenot.com/categories/"
    myRequest = urllib2.Request(myUrl, headers=headers)
    myResponse = urllib2.urlopen(myRequest)
    myPage = myResponse.read()
    soup = BeautifulSoup(myPage)
    categoryColumnsList = soup.find_all('ul', {'class': 'categories'})
    cates = []
    for categoryColumn in categoryColumnsList:
        categoryList = categoryColumn.find_all('li')
        for category in categoryList:
            cate = [category.a['class'][0], category.a['href']]
            cates.append(cate)
    return cates # e.g. [[category_name,categroy_url],...,[category_name,categroy_url]]


def GetCatesFromCates(cates):
    for cate in cates:
        try:
            baseURL = "http://www.retailmenot.com"
            IP = socket.inet_ntoa(struct.pack('>I', random.randint(1, 0xffffffff)))
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6',
                'Referer': 'http://www.google.com/',
                'X-Forwarded-For': IP
            }
            myUrl = baseURL + cate[1]
            print myUrl
            myRequest = urllib2.Request(myUrl, headers=headers)
            try:
                myResponse = urllib2.urlopen(myRequest)
            except Exception, e:
                print e
                raise Exception('network') # when a network error raises, we reloop this url by passing 'network' as an error message.
            if 'coupons' not in myResponse.geturl():
                print 'URL Error-------------!'
                raise Exception('Bad URL') # sometimes the category link leads to a special deal page, so we skip this category.
            myPage = myResponse.read()
            soup = BeautifulSoup(myPage)
            dbcates = [] # initialize a category list to insert to db.
            categoryColumnsList = soup.find_all('ul', {'class': 'tags'})
            for categoryColumn in categoryColumnsList:
                categoryList = categoryColumn.find_all('li')
                for category in categoryList:
                    new_cate = [category.a.text.encode("ascii", "ignore"), category.a['href']]
                    dbcates.append(new_cate)
                    q = dbc.insert_retailmenot_categories(dbcates)
                    for c in q:
                        cates.append(c)
            storeList = soup.find_all('ol', {'class': 'topList'})[0].find_all('li')
            store_list = []
            for store in storeList:
                store_list.append({'store': store.text.replace(' ', '').encode("ascii", "ignore").replace("\"", ""),
                                   'link': store.a['href']})
            dbc.fuckthestoresandcates(store_list, cate)
            if not soup.find_all('div', {'class': 'rmn-logo'}):
                print 'human check'
                raise Exception(cate, cates)
        except:
            print 'unknown'
            raise Exception(cate, cates)
    print 'finished'
    return cates


def main():
    a = []
    error1 = 0
    while True:
        try:
            GetCatesFromCates(a)
        except Exception, e:
            # print e
            print error1
            error1 += 1
            a = e[1]
            cate = e[0]
            for i in range(0, a.index(cate)):
                a.remove(a[0])
            if error1 > 3:
                error1 = 0
                a.remove(a[0])
            fileOutHandle = open('log.txt', 'w')
            fileOutHandle.write(str(a))
            fileOutHandle.close()
            print 'again..................................................................', error1


dbc = DBConnection()

def Coupon(a, b):
    for i in range(a, b):
        try:
            GetAllCouponFromStore(i)
        except:
            print 'Error at', i
            raise Exception(i)
    raw_input()


def cool():
    errorC = 0
    start = 0
    while True:
        try:
            Coupon(start, 38200)
        except Exception, e:
            errorC += 1
            # print 'Start at ', e[0]
            if errorC > 5:
                errorC = 0
                start = e[0] +1
            else:
                start = e[0]

main()
cool()
