# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.10)
# Database: zulily3
# Generation Time: 2014-04-28 21:05:42 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table brand
# ------------------------------------------------------------

DROP TABLE IF EXISTS `brand`;

CREATE TABLE `brand` (
  `b_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `brandname` varchar(100) DEFAULT NULL,
  `brandurl` text,
  PRIMARY KEY (`b_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `c_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `url` text,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table category_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category_event`;

CREATE TABLE `category_event` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(11) unsigned NOT NULL,
  `eid` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `c` (`cid`),
  KEY `e` (`eid`),
  CONSTRAINT `e` FOREIGN KEY (`eid`) REFERENCES `event` (`e_id`),
  CONSTRAINT `c` FOREIGN KEY (`cid`) REFERENCES `category` (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event`;

CREATE TABLE `event` (
  `e_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_title` varchar(500) DEFAULT NULL,
  `end_date` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `url` text,
  `isnew` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`e_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table p_e
# ------------------------------------------------------------

DROP TABLE IF EXISTS `p_e`;

CREATE TABLE `p_e` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `e_id` int(11) unsigned DEFAULT NULL,
  `p_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `e_id` (`e_id`),
  KEY `p_id` (`p_id`),
  CONSTRAINT `p_e_ibfk_2` FOREIGN KEY (`p_id`) REFERENCES `product` (`p_id`),
  CONSTRAINT `p_e_ibfk_1` FOREIGN KEY (`e_id`) REFERENCES `event` (`e_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `p_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `old_price` varchar(20) DEFAULT NULL,
  `special_price` varchar(20) DEFAULT NULL,
  `item_name` text,
  `is_out_of_stock` tinyint(1) DEFAULT NULL,
  `b_id` int(11) unsigned DEFAULT NULL,
  `purl` text,
  PRIMARY KEY (`p_id`),
  KEY `b_id` (`b_id`),
  CONSTRAINT `product_ibfk_2` FOREIGN KEY (`b_id`) REFERENCES `brand` (`b_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table readytoship
# ------------------------------------------------------------

DROP TABLE IF EXISTS `readytoship`;

CREATE TABLE `readytoship` (
  `r_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `e_id` int(11) unsigned DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`r_id`),
  KEY `e_id` (`e_id`),
  CONSTRAINT `readytoship_ibfk_1` FOREIGN KEY (`e_id`) REFERENCES `event` (`e_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
