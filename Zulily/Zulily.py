__author__ = 'TTc9082'
import urllib2
import urllib
import time
import MySQLdb
import random
import socket
import struct
from bs4 import BeautifulSoup
import datetime
import calendar
import os.path
import threading
import cookielib
import json


def useproxy(proxy):
    proxy_handler = urllib2.ProxyHandler(proxy)
    cj = cookielib.LWPCookieJar()
    cookie_support = urllib2.HTTPCookieProcessor(cj)
    opener = urllib2.build_opener(cookie_support, urllib2.HTTPHandler, proxy_handler)
    urllib2.install_opener(opener)


class DBConnection():
    def __init__(self, db):
        self.conn = MySQLdb.connect(host='localhost', user='root',
                                    passwd='', db=db)

    def insert_product(self, product):
        cursor = self.conn.cursor()
        sql = 'select p_id from product where item_name = "%s"' % (product['name'])
        try:
            cursor.execute(sql)
        except Exception, e:
            print e
        pid = cursor.fetchall()
        print pid
        cursor.close()
        self.conn.commit()
        if pid == ():
            cursor = self.conn.cursor()
            sql = 'insert into product(`old_price`, `item_name`, `is_out_of_stock`, `b_id`,`purl`, `special_price`) values ("%s","%s","%d", "%d", "%s", "%s")' % (
                product['old'], product['name'], product['oos'], product['bid'], product['url'], product['special'])
            try:
                cursor.execute(sql)
                p_id = cursor.lastrowid
            except Exception, e:
                print e
            cursor.close()
            self.conn.commit()
        else:
            p_id = pid[0][0]
            print 'pid:', p_id
        return p_id

    def insert_event(self, event):
        cursor = self.conn.cursor()
        sql = 'select e_id from event where url = "%s" and time = "%d"' % (event['url'], event['date'])
        try:
            cursor.execute(sql)
        except Exception, e:
            print e
        eid = cursor.fetchall()
        print eid
        cursor.close()
        self.conn.commit()
        if eid == ():
            cursor = self.conn.cursor()
            sql = 'insert into event(`event_title`, `end_date`, `time`, `url`) values ("%s","%d","%d", "%s")' % (
                event['title'], event['endtime'], event['date'], event['url'])
            try:
                cursor.execute(sql)
                e_id = cursor.lastrowid
            except Exception, e:
                print e
                try:
                    print 'try to insert with error title.'
                    sql = 'insert into event(`event_title`, `end_date`, `time`, `url`) values ("%s","%d","%d", "%s")' % (
                        'Error!', event['endtime'], event['date'], event['url'])
                    cursor.execute(sql)
                    e_id = cursor.lastrowid
                except Exception, e:
                    print e
            cursor.close()
            self.conn.commit()
        else:
            e_id = eid[0][0]
        print 'eid:', e_id
        return e_id

    def get_or_insert_brand(self, brand):
        cursor = self.conn.cursor()
        sql = 'select b_id from brand where brandname = "%s"' % (brand[0])
        try:
            cursor.execute(sql)
        except Exception, e:
            print e
        bid = cursor.fetchall()
        print bid
        cursor.close()
        self.conn.commit()
        if bid == ():
            print 'create new store of ', brand[0]
            cursor = self.conn.cursor()
            sql = 'insert into brand(brandname, brandurl) values ("%s", "%s")' % (brand[0], brand[1])
            try:
                cursor.execute(sql)
                bid = cursor.lastrowid
            except Exception, e:
                print e
            cursor.close()
            self.conn.commit()
            print 'bid:', bid
            b_id = bid
        else:
            b_id = bid[0][0]
        print 'bid', b_id
        return b_id

    def insert_category(self, category):
        cursor = self.conn.cursor()
        sql = 'select c_id from category where title = "%s"' % (category.split('/')[3])
        try:
            cursor.execute(sql)
        except Exception, e:
            print e
        cid = cursor.fetchall()
        print cid
        cursor.close()
        self.conn.commit()
        if cid == ():
            print 'create new store of ', category.split('/')[3]
            cursor = self.conn.cursor()
            sql = 'insert into category(title, url) values ("%s", "%s")' % (category.split('/')[3], category)
            try:
                cursor.execute(sql)
                c_id = cursor.lastrowid
            except Exception, e:
                print e
            cursor.close()
            self.conn.commit()
        else:
            c_id = cid[0][0]
            print 'cid:', c_id
        return c_id


    def insert_e_c_relation(self, eid, cid):
        cursor = self.conn.cursor()
        sql = 'insert into category_event(eid, cid) values ("%d", "%d")' % (eid, cid)
        try:
            cursor.execute(sql)
        except Exception, e:
            print e
        cursor.close()
        self.conn.commit()

    def insert_price(self, special_price):
        cursor = self.conn.cursor()
        sql = 'insert into price(p_id, `size`,`special_price`) values ("%d", "%s", "%d")' % \
              (special_price['p_id'], special_price['size'], special_price['price'])
        try:
            cursor.execute(sql)
        except Exception, e:
            print e
        cursor.close()
        self.conn.commit()

    def get_item_id(self, item_name):
        cursor = self.conn.cursor()
        sql = 'select p_id from product where item_name = "%s"' % item_name
        try:
            cursor.execute(sql)
        except Exception, e:
            print e
        pid = cursor.fetchall()
        cursor.close()
        self.conn.commit()
        p_id = pid[0][0]
        print 'pid:', p_id
        return p_id

    def get_or_insert_type(self, type_name):
        cursor = self.conn.cursor()
        sql = 'select pt_id from ptype where title = "%s"' % type_name
        try:
            cursor.execute(sql)
        except Exception, e:
            print e
        ptid = cursor.fetchall()
        print ptid
        cursor.close()
        self.conn.commit()
        if ptid == ():
            print 'create new store of ', type_name
            cursor = self.conn.cursor()
            sql = 'insert into ptype(title) values ("%s")' % type_name
            try:
                cursor.execute(sql)
                pt_id = cursor.lastrowid
            except Exception, e:
                print e
            cursor.close()
            self.conn.commit()
        else:
            pt_id = ptid[0][0]
            print 'ptid:', pt_id
        return pt_id

    def insert_relationship_product_type(self, p_id, pt_id):
        cursor = self.conn.cursor()
        sql = 'insert into p_t(p_id, pt_id) values ("%d", "%d")' % \
              (p_id, pt_id)
        try:
            cursor.execute(sql)
        except Exception, e:
            print e
        cursor.close()
        self.conn.commit()

    def insert_p_e(self, p_id, eid):
        cursor = self.conn.cursor()
        sql = 'insert into p_e(p_id, e_id) values ("%d", "%d")' % \
              (p_id, eid)
        try:
            cursor.execute(sql)
        except Exception, e:
            print e
        cursor.close()
        self.conn.commit()

    def insert_rts_num(self, eid, num):
        cursor = self.conn.cursor()
        sql = 'insert into readytoship(e_id, count) values ("%d", "%d")' % \
              (eid, num)
        try:
            cursor.execute(sql)
        except Exception, e:
            print e
        cursor.close()
        self.conn.commit()


def main_category():
    categories = ['http://www.zulily.com/girls/?ref=header', 'http://www.zulily.com/boys/?ref=header',
                  'http://www.zulily.com/shoes/?ref=header',
                  'http://www.zulily.com/women/?ref=header', 'http://www.zulily.com/home/?ref=header',
                  'http://www.zulily.com/baby-maternity/?ref=header',
                  'http://www.zulily.com/toys-playtime/?ref=header']
    return categories


def login(email):
    postdata = {'firstname': "ttc3",
                'lastname': "1std",
                'email': email,
                'password': "111111",
                'confirmation': "111111"}
    postdata1 = urllib.urlencode(postdata)
    posturl = 'https://www.zulily.com/auth/create'
    request = urllib2.Request(posturl, postdata1)
    print request
    response = urllib2.urlopen(request)
    myPage = response.read()
    soup = BeautifulSoup(myPage)
    items = soup.find('li', {'class': 'first'})
    print items


def browse(url):
    IP = socket.inet_ntoa(struct.pack('>I', random.randint(1, 0xffffffff)))
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6',
        'Referer': 'http://www.zulily.com/',
        'X-Forwarded-For': IP,
        'Cookie': "zuid=r03-60fa7c1b-53572026-ff88-961c1b1c-53572026f3b63; cc2=US%7CUSD; btpdb.bb0DvYz.X2J0X3Jt=MjQ2NjQ1OCwyNDQwMTM2; frontend=3bkslamtulst5noecud9aibqk4; zuname=czoxOToiMTJhMzNkMUAxMjBlbmNvA7czo2OiIyMjU5ODMxczIzLmNvbSI7; zud7=YToxMTp7aTowO3M6ODoiMzBlbmNvA7czo2OiIyMjU5ODU3MTYwMzUiO2k6MTtzOjI6ImRzIjtpOjI7czo0OiJhZGZzIjtpOjM7czo5OiI5MDgzMzEwNjMiO2k6NDtzOjg6ImRhZGZzNzQ4IjtpOjU7czoxOToiMTJhMzNkMUAxMjMxczIzLmNvbSI7aTo2O047aTo3O3M6MTk6IjIwMTQtMDQtMjIgMTk6MjI6NTciO2k6ODtOO2k6OTtOO2k6MTA7czowOiIiO30%3D; cid=908331063; unlimited_banner_visits=a%3A3%3A%7Bs%3A6%3A%22visits%22%3Bi%3A2%3Bs%3A7%3A%22created%22%3Bs%3A10%3A%222014-04-22%22%3Bs%3A12%3A%22last_visited%22%3Bs%3A10%3A%222014-04-24%22%3B%7D; btpdb.bb0DvYz.dGZjLjMxMjMzMA=REFZUw; btpdb.bb0DvYz.dGZjLjM1NzAxNQ=U0VTU0lPTg; btpdb.bb0DvYz.Y2FwcGVk=dHJ1ZQ; btpdb.bb0DvYz.aHNjcGlk=; __utma=14735037.1292809501.1398219147.1398219147.1398348796.2; __utmb=14735037.20.9.1398350986835; __utmc=14735037; __utmz=14735037.1398219147.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmv=14735037.|2=split%3A%20shop_by_age=show_shop_by_age=1"
    }
    myUrl = url
    myRequest = urllib2.Request(myUrl, headers=headers)
    myResponse = urllib2.urlopen(myRequest, timeout=3)
    block = 'www.zulily.com/z/' in myResponse.geturl()
    print myResponse.geturl()
    if block:
        raise Exception('block')
    myPage = myResponse.read()
    return myPage


def zulily_get_events_from_main_category(category):
    event_list = []
    myPage = browse(category)
    soup = BeautifulSoup(myPage)
    events = soup.find_all('a', {'class': 'wrapped-link'})
    for event in events:
        print event['href'], len(events)
        event_name = event.find('span', {'class': 'category-name'}).text.replace('\n', '').encode('ascii', 'ignore').replace('"', "")
        event_end_date = event.find('span', {'class': 'start-end-date'}).text
        if 'New' in event_end_date:
            event_list.append([event['href'].split('?')[0], event_name])
    print 'event_list:', event_list
    return event_list


def zulily_get_types_from_event(event, dbc, category):
    type_list = []
    today = datetime.date.today()
    myPage = browse(event[0])
    soup = BeautifulSoup(myPage)
    types = soup.find_all('ul', {'name': 'category'})[0].find_all('li')
    endtime_pre = soup.find('div', {'class': 'end-date'}).text
    if 'day' in endtime_pre:
        delta = int(endtime_pre.strip().split(' ')[3])
        endtime = datetime.date.today() + datetime.timedelta(days=delta)
    else:
        endtime = datetime.date.today()
    insert_event = {'title': event[1], 'date': int(today.strftime('%Y%m%d')),
                    'endtime': int(endtime.strftime('%Y%m%d')), 'url': event[0]}
    eid = int(dbc.insert_event(insert_event))
    dbc.insert_e_c_relation(eid, category)
    for type in types:
        if type['data-value']:
            type_list.append(type['data-value'])
    return type_list, eid


def zulily_get_items_from_type(event, ztype):
    myUrl = event[0] + "&category=" + ztype
    myPage = browse(myUrl)
    soup = BeautifulSoup(myPage)
    items = soup.find('ul', {'id': 'products-list'}).find_all('li', {'class': 'item'})
    return items


def zulily_get_items_from_all(event):
    final_items = []
    url = event[0].replace(' ', '%20')
    print url
    myPage = browse(url)
    soup = BeautifulSoup(myPage)
    pagecounts = soup.find_all('div', {'id': 'pagination'})
    if not pagecounts:
        print 'hi'
        items = soup.find('ul', {'id': 'products-list'}).find_all('li', {'class': 'item'})
        for item in items:
            try:
                purl = item.find('a', {'class': 'product-image'})['href']
                # item_name = item.meta['content'].encode('ascii', 'ignore').replace('"', "")
                item_name = item.find('a', {'class': 'product-image'})['title'].encode('ascii', 'ignore').replace('"', "")
                is_out_of_stock = 0 if item['data-available-sizes'] else 1
                item_old_price = u'0'
                item_special_price = item.find('div', {'class': 'special-price'}).text.strip().replace('$', '')
                if item.find('div', {'class': 'old-price'}):
                    try:
                        item_old_price = item.find('div', {'class': 'old-price'}).text.strip().replace('$', '')
                    except Exception, e:
                        print e
                final_items.append([purl, item_name, is_out_of_stock, item_old_price, item_special_price])
            except Exception, e:
                print e
                print 'Fail, maybe an ad.'
    else:
        counts = pagecounts[0].find_all('a')
        last = int(counts[-2].text)
        for i in range(1, last + 1):
            pagesurl = url + '?page=' + str(i)
            print pagesurl
            myPage = browse(pagesurl)
            soup = BeautifulSoup(myPage)
            items = soup.find('ul', {'id': 'products-list'}).find_all('li', {'class': 'item'})
            for item in items:
                try:
                    purl = item.find('a', {'class': 'product-image'})['href']
                    # item_name = item.meta['content'].encode('ascii', 'ignore').replace('"', "")
                    item_name = item.find('a', {'class': 'product-image'})['title'].encode('ascii', 'ignore').replace('"', "")
                    is_out_of_stock = 0 if item['data-available-sizes'] else 1
                    item_old_price = u'0'
                    item_special_price = item.find('div', {'class': 'special-price'}).text.strip().replace('$', '')
                    if item.find('div', {'class': 'old-price'}):
                        try:
                            item_old_price = item.find('div', {'class': 'old-price'}).text.strip().replace('$', '')
                        except Exception, e:
                            print e
                    final_items.append([purl, item_name, is_out_of_stock, item_old_price, item_special_price])
                except Exception, e:
                    print e
                    print 'Fail, maybe an ad.'
    return final_items

def zulily_insert_relationship_between_item_and_type(ztype, item, dbc):
    item_name = item.meta['content'].encode('ascii', 'ignore').replace('"', "")
    p_id = dbc.get_item_id(item_name)
    pt_id = dbc.get_or_insert_type(ztype)
    dbc.insert_relationship_product_type(p_id, pt_id)

def combine():
    events_dict_final = {}
    for file_path in ["./boys.json", "./baby-maternity.json", "./girls.json", "./home.json", "./shoes.json", "./toys-playtime.json", "./women.json"]:
        f = file(file_path)
        events_dict = json.load(f)
        f.close()
        events_dict_final.update(events_dict)
        with open('all_event_products.json', 'w') as outfile:
            json.dump(events_dict_final, outfile)

def zulily_insert_product_from_item(item, eid, dbc):
    insert_p = {}
    # item_special_offer = item[3]
    # item_detail_url = item.find('a', {'class': 'product-image'})['href']
    item_detail_url = item[0]
    print item_detail_url
    # try:
    #     myPage = browse(item_detail_url)
    # except Exception, e:
    #     f = file('backup.json')
    #     p_dict = json.load(f)
    #     try:
    #         p_dict[eid].append(item)
    #     except:
    #         p_dict.update({eid: [item]})
    #     with open('backup.json', 'w') as outfile:
    #         json.dump(p_dict, outfile)
    #     if e.message =='block':
    #         print 'blocked and logged.'
    #         raise Exception('block')
    #     else:
    #         print 'unknow error.'
    #         raise Exception('s')
    # soup = BeautifulSoup(myPage)
    # item_brand = soup.find('a', {'id': 'brand-name'})
    # bid = dbc.get_or_insert_brand([item_brand.text, item_brand['href']]) if item_brand else 0
    search_page = 'http://www.zulily.com/' + item[1].replace(' ', '%20')
    page = browse(search_page)
    soup = BeautifulSoup(page)
    p_name = soup.find('div', {'class': 'product-name'}).a.text
    if not p_name:
        print 'no result for', p_name
    if soup.find('div', {'class': 'product-name'}).strong:
        b_name = soup.find('div', {'class': 'product-name'}).strong.text.strip().encode('ascii', 'ignore').replace('"', "")
        bid = dbc.get_or_insert_brand([b_name, ''])
    else:
        bid = 0
    insert_p['old'] = item[3]
    insert_p['special'] = item[4]
    insert_p['name'] = item[1]
    insert_p['oos'] = item[2]
    insert_p['url'] = item_detail_url
    insert_p['bid'] = bid
    p_id = dbc.insert_product(insert_p)
    dbc.insert_p_e(p_id, int(eid))
    # price = soup.find('span', {'class': 'price'})
    # sizes = soup.find_all('span', {'class': 'js-sb-item'})
    # if '-' in price.text:
    #     all_out_of_stocks = soup.find_all('div', {'class': 'size-out-of-stock'})
    #     if all_out_of_stocks:
    #         for out_of_stock in all_out_of_stocks:
    #             size = out_of_stock.text.strip().split(' ')[1].strip()
    #             oos = {'size': size, 'p_id': p_id, 'price': 0}
    #             dbc.insert_price(oos)
    #     out_of_stocks = soup.find_all('span', {'class': 'out-of-stock'})
    #     if out_of_stocks:
    #         for out_of_stock in out_of_stocks:
    #             size = out_of_stock.find('span', {'class': 'label-name'}).text
    #             oos = {'size': size, 'p_id': p_id, 'price': 0}
    #             dbc.insert_price(oos)
    #     for special_price in sizes:
    #         size = special_price.text.strip().split('-')[0]
    #         ssprice = float(special_price.text.strip().split('-')[1].strip()[1:])
    #         s_price = {'p_id': p_id, 'price': ssprice, 'size': size}
    #         dbc.insert_price(s_price)
    # else:
    #     all_out_of_stocks = soup.find_all('div', {'class': 'size-out-of-stock'})
    #     if all_out_of_stocks:
    #         for out_of_stock in all_out_of_stocks:
    #             size = out_of_stock.text.strip().split(' ')[1].strip()
    #             oos = {'size': size, 'p_id': p_id, 'price': 0}
    #             dbc.insert_price(oos)
    #     out_of_stocks = soup.find_all('span', {'class': 'out-of-stock'})
    #     if out_of_stocks:
    #         for out_of_stock in out_of_stocks:
    #             size = out_of_stock.find('span', {'class': 'label-name'}).text
    #             oos = {'size': size, 'p_id': p_id, 'price': 0}
    #             dbc.insert_price(oos)
    #     for special_price in sizes:
    #         size = special_price.text.strip().split('-')[0]
    #         ssprice = float(price.text[1:])
    #         s_price = {'p_id': p_id, 'price': ssprice, 'size': size}
    #         dbc.insert_price(s_price)
    return 0


def readytoship():
    all = {}
    dbc = DBConnection('zulily3')
    today = datetime.date.today()
    urllist = ['http://www.zulily.com/e/rts-girls-011413.html', 'http://www.zulily.com/e/rts-boys-011413.html', 'http://www.zulily.com/e/rts-women-011413.html', 'http://www.zulily.com/e/ready-to-ship-shoes-012813.html',
             'http://www.zulily.com/e/rts-baby-and-maternity-011413.html', 'http://www.zulily.com/e/rts-toys-and-playtime-011413.html', 'http://www.zulily.com/e/rts-home-and-gear-011413.html']
    for url in urllist:
        page = browse(url)
        soup = BeautifulSoup(page)
        pagecounts = soup.find_all('div', {'id': 'pagination'})
        if not pagecounts:
            last = 1
        else:
            counts = pagecounts[0].find_all('a')
            last = int(counts[-2].text)
        category_name = url.split('/')[4][:-12]
        print last
        all[category_name] = []
        for i in range(1, last+1):
            print i
            pagesurl = url + '?page=' + str(i)
            all[category_name].append(pagesurl)
        event = {'title': str(today.strftime('%Y%m%d'))+category_name, 'url': url, 'endtime':0, 'date':int(today.strftime('%Y%m%d')), 'is_new':0}
        eid = int(dbc.insert_event(event))
        lastpage = browse(url+'?page='+str(last))
        lastsoup = BeautifulSoup(lastpage)
        num = (last-1)*150 + len(lastsoup.find_all('li', {'class': 'item'}))
        dbc.insert_rts_num(eid, num)
    print all
    return all


def get_products(category, name):
    products = {}
    dbc = DBConnection('zulily3')
    cid = dbc.insert_category(category)
    events = zulily_get_events_from_main_category(category)
    for event in events:
        type_list, eid = zulily_get_types_from_event(event, dbc, cid)
        zitems = zulily_get_items_from_all(event)
        products[eid] = zitems
    jsonname = name + '.json'
    with open(jsonname, 'w') as outfile:
        json.dump(products, outfile)


class get_products_and_export_to_json(threading.Thread):
    def __init__(self, categoryurl):
        threading.Thread.__init__(self)
        self.url = categoryurl
        self.jsonname = categoryurl.split('/')[3]

    def run(self):
        print "start thread", self.url
        get_products(self.url, self.jsonname)
        print "\n"


# useproxy({})
emails = ['dfcc@yo.com', 'ryx2c3@yoo.com', '144@ydhoo.com', 'df3o@yao.com', 's1asc11@y1o.com',
          'rys123@y1oo.com', 'rsxd3@yaSo.com']

def ready():
    threads = []
    categories = main_category()
    for category in categories:
        thread = get_products_and_export_to_json(category)
        thread.start()
        threads.append(thread)

    for thread in threads:
        thread.join()


def main():
    threads = []
    f = file('all_event_products.json')
    final_dict = json.load(f)
    for eid in final_dict:
        thread = handling_products(eid, final_dict)
        thread.start()
        threads.append(thread)
        print len(threading.enumerate())
        while True:
            if(len(threading.enumerate()) < 20):
                break
            else:
                print 'sleep'
                time.sleep(10)
    for thread in threads:
        thread.join()


class handling_products(threading.Thread):
    def __init__(self, eid, final_dict):
        self.dbc = DBConnection('zulily3')
        threading.Thread.__init__(self)
        self.product_list = final_dict[eid]
        self.eid = eid

    def run(self):
        products(self.eid, self.product_list)
        print 'handling the events.', self.eid

def products(eid, product_list):
    for product in product_list:
        dbc = DBConnection('zulily3')
        zulily_insert_product_from_item(product, eid, dbc)
        # login(random.choice(emails))
        # pp = random.choice(proxylist)
        # useproxy({'http': pp})
        # try:
        #     zulily_insert_product_from_item(product, eid, dbc)
        # except Exception, e:
        #     if e.message == 'block':
        #         proxylist.remove(pp)
        #         print 'remove', pp
        #     print 'pass',e
        #     print proxylist
    # if type_list:
    #     for ztype in type_list:
    #         t_attempts = 0
    #         while t_attempts < 3:
    #             try:
    #                 zitems = zulily_get_items_from_type(event, ztype)
    #                 break
    #             except:
    #                 t_attempts += 1
    #             for item in zitems:
    #                 p_attempts = 0
    #                 while p_attempts < 3:
    #                     try:
    #                         zulily_insert_relationship_between_item_and_type(ztype, item, dbc)
    #                         break
    #                     except Exception, e:
    #                         p_attempts += 1
    #                         print '2', e


        
# ready()
# combine()
# main()
readytoship()