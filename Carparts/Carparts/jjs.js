var loadVar = null;
var el_ymm = document.getElementById("ymm");
var onChangeEvent = false;
var ymmSavedVehicleLastFocus = null;
if (el_ymm != null) {
    load_ymm_dropdown()
}
$(this).ajaxComplete(function () {
    if (loadVar != null && loadVar.options.length > 1) {
        loadVar.disabled = false
    }
});
function load_ymm_dropdown() {
    partfinder_init();
    var a = document.getElementById("ymm_submit");
    var b = document.getElementById("ymm");
    var c = document.getElementById("ymm_year");
    var d = document.getElementById("ymm_make");
    var e = document.getElementById("ymm_model");
    var f = document.getElementById("ymm_submodel");
    var g = document.getElementById("ymm_engine");
    var h = document.getElementById("selected_year");
    var i = document.getElementById("selected_make");
    var j = document.getElementById("selected_model");
    var k = document.getElementById("selected_submodel");
    var l = document.getElementById("selected_engine");
    if (b != null) {
        d.disabled = true;
        d.disabled = true;
        e.disabled = true;
        f.disabled = true;
        g.disabled = true;
        var m = document.getElementById("myvehicles");
        var n = document.getElementById("headlineYear");
        var o = document.getElementById("headlineMake");
        var p = document.getElementById("headlineModel");
        var q = document.getElementById("headlineSubmodel");
        var r = document.getElementById("headlineEngine");
        var s = document.getElementById("nosubmit");
        var t = document.getElementById("fitchecker");
        if (m != null) {
            var u = document.getElementById("myvehiclesYear").value;
            var v = document.getElementById("myvehiclesMake").value;
            var w = document.getElementById("myvehiclesModel").value;
            var x = document.getElementById("myvehiclesSubmodel").value;
            var y = document.getElementById("myvehiclesEngine").value;
            onChangeEvent = false;
            if (y !== "") {
                ymmSavedVehicleLastFocus = "engine"
            } else if (x !== "") {
                ymmSavedVehicleLastFocus = "submodel"
            } else if (w !== "") {
                ymmSavedVehicleLastFocus = "model"
            }
            loadVar = c;
            load_ymm_years(u);
            loadVar = d;
            load_ymm_makes(u, v);
            loadVar = e;
            load_ymm_models(u, v, w);
            loadVar = f;
            load_ymm_submodels(u, v, w, x, function () {
                if (ymmSavedVehicleLastFocus == "submodel") {
                    ymmSavedVehicleLastFocus = null;
                    $("#ymm_submodel").focus()
                }
            });
            loadVar = g;
            load_ymm_engines(u, v, w, x, y, function () {
                if (ymmSavedVehicleLastFocus == "engine") {
                    ymmSavedVehicleLastFocus = null;
                    $("#ymm_engine").focus()
                }
            })
        } else if (n != null) {
            loadVar = c;
            load_ymm_years(n.value);
            if (o != null) {
                loadVar = d;
                load_ymm_makes(n.value, o.value);
                if (p != null) {
                    loadVar = e;
                    load_ymm_models(n.value, o.value, p.value)
                } else {
                    loadVar = e;
                    load_ymm_models(n.value, o.value)
                }
            } else {
                loadVar = d;
                load_ymm_makes(n.value)
            }
        } else {
            loadVar = c;
            if (h != null) {
                if (h.value != '') {
                    load_ymm_years(h.value);
                    load_ymm_makes(h.value, i.value);
                    if (j.value != '') {
                        load_ymm_models(h.value, i.value, j.value)
                    } else {
                        load_ymm_models(h.value, i.value)
                    }
                } else {
                    load_ymm_years()
                }
            } else {
                load_ymm_years()
            }
        }
        $(c).change(function () {
            loadVar = d;
            onChangeEvent = true;
            load_ymm_makes(this.value);
            yearChange(this.value)
        });
        $(d).change(function () {
            loadVar = e;
            onChangeEvent = true;
            load_ymm_models(c.value, this.value);
            makeChange(this.value)
        });
        $(e).change(function () {
            loadVar = f;
            onChangeEvent = true;
            load_ymm_submodels(c.value, d.value, this.value);
            modelChange(this.value)
        });
        $(f).change(function () {
            loadVar = g;
            onChangeEvent = true;
            load_ymm_engines(c.value, d.value, e.value, this.value);
            submodelChange(this.value)
        });
        $(g).change(function () {
            if (this.value != "") {
                loadVar = null;
                if (s == null) {
                    onChangeEvent = true;
                    save_vehicle(c.value, d.value, e.value, f.value, this.value)
                }
            }
            engineChange(this.value)
        });
        $(a).click(function () {
            if (c.value == '') {
                alert('Please select a year');
                return false
            } else if (d.value == '') {
                alert('Please select a make');
                return false
            } else if (e.value == '') {
                alert('Please select a model');
                return false
            } else if (t != null) {
                ManageVehicleFits()
            } else if (s != null) {
                save_vehicle(c.value, d.value, e.value, '', '')
            } else {
                save_vehicle(c.value, d.value, e.value, f.value, g.value)
            }
        });
        return false
    }
}
function setShopByVehicleCookies() {
    var a = true;
    USAPCM.setCookie('anlytxsbv', a, 60)
}
function setVehicleSelectionPageRef() {
    var a = '';
    var b = document.getElementById("page_ref").value;
    USAPCM.setCookie('vsw_pr', b, 60)
}
function save_vehicle(c, d, e, f, g) {
    encoded_make = escape(encode(d.toLowerCase()));
    encoded_model = escape(encode(e.toLowerCase()));
    encoded_submodel = escape(encode(f.toLowerCase()));
    encoded_engine = escape(encode(g.toLowerCase()));
    var h = document.getElementById("nosubmit");
    var i = false;
    if (h != null) {
        if (h != "") {
            i = true
        }
    }
    if (!i) {
        document.forms.ymm.year.disabled = true;
        document.forms.ymm.make.disabled = true;
        document.forms.ymm.model.disabled = true;
        document.forms.ymm.submodel.disabled = true;
        document.forms.ymm.engine.disabled = true;
        document.forms.ymm.ymm_submit.disabled = true
    }
    loadVar = null;
    setShopByVehicleCookies();
    var j = document.getElementById("page_ref");
    if (j != null) {
        setVehicleSelectionPageRef()
    }
    $.get("/managevehicle/?status=add&year=" + c + "&make=" + escape(d.toLowerCase()) + "&model=" + escape(encode(e.toLowerCase())) + '&submodel=' + escape(f.toLowerCase()) + '&engine=' + escape(g.toLowerCase()), {}, function (a, b) {
        if (b == "success") {
            if (!a) {
                window.location.reload()
            } else {
                vehicle_params = c + '/' + encoded_make + '/' + encoded_model;
                if (encoded_submodel.length > 0) {
                    vehicle_params += '/' + encoded_submodel
                }
                if (encoded_engine.length > 0) {
                    vehicle_params += '/' + encoded_engine
                }
                if ($("#ymm").attr('action').indexOf("/vehicle/") != -1) {
                    $("#ymm").attr({'action': $("#ymm").attr('action') + vehicle_params + '.html'});
                    $("#ymm").submit()
                } else if ($("#ymm").attr('action').indexOf("/search/") != -1) {
                    $("#ymm").attr({'action': $("#ymm").attr('action')});
                    $("#ymm").submit()
                } else {
                    if (window.removeVehicleQuery) {
                        window.parent.location.href = removeVehicleQuery(0)
                    } else {
                        window.parent.location.href
                    }
                }
            }
        }
    })
}
function load_ymm_years(b) {
    document.forms.ymm.ymm_year.options.length = 1;
    document.forms.ymm.ymm_make.options.length = 1;
    document.forms.ymm.ymm_model.options.length = 1;
    document.forms.ymm.ymm_submodel.options.length = 1;
    document.forms.ymm.ymm_engine.options.length = 1;
    document.forms.ymm.ymm_make.disabled = true;
    document.forms.ymm.ymm_model.disabled = true;
    document.forms.ymm.ymm_submodel.disabled = true;
    document.forms.ymm.ymm_engine.disabled = true;
    select_year_text = document.forms.ymm.ymm_year.options[0].text;
    document.forms.ymm.ymm_year.options[0].text = "Loading years...";
    $.get("/partfinder/year/0", {}, function (a) {
        a = $.trim(a);
        a = a.split("|");
        for (i = 0; i < a.length; i++) {
            document.forms.ymm.ymm_year.options[i + 1] = new Option(a[i], a[i]);
            if (b != "") {
                if (a[i] == b) {
                    document.forms.ymm.ymm_year.selectedIndex = i + 1;
                    document.forms.ymm.ymm_year.disabled = false;
                    document.forms.ymm.ymm_make.disabled = false
                }
            }
        }
        document.forms.ymm.ymm_year.options[0].text = select_year_text
    })
}
function load_ymm_makes(b, c) {
    document.forms.ymm.ymm_make.options.length = 1;
    document.forms.ymm.ymm_model.options.length = 1;
    document.forms.ymm.ymm_submodel.options.length = 1;
    document.forms.ymm.ymm_engine.options.length = 1;
    document.forms.ymm.ymm_make.disabled = true;
    document.forms.ymm.ymm_model.disabled = true;
    document.forms.ymm.ymm_submodel.disabled = true;
    document.forms.ymm.ymm_engine.disabled = true;
    select_make_text = document.forms.ymm.ymm_make.options[0].text;
    if (b != "") {
        document.forms.ymm.ymm_make.options[0].text = "Loading makes...";
        $.get("/partfinder/make/year/" + b + "/0", {}, function (a) {
            a = $.trim(a);
            a = a.split("|");
            for (i = 0; i < a.length; i++) {
                document.forms.ymm.ymm_make.options[i + 1] = new Option(a[i], encodeURIComponent(encodeWithCase(a[i])));
                if (c != "") {
                    if (a[i] == c) {
                        document.forms.ymm.ymm_make.selectedIndex = i + 1;
                        document.forms.ymm.ymm_make.disabled = false;
                        document.forms.ymm.ymm_model.disabled = false
                    }
                }
            }
            document.forms.ymm.ymm_make.options[0].text = select_make_text
        })
    } else {
        document.forms.ymm.ymm_make.disabled = true;
        document.forms.ymm.ymm_model.disabled = true;
        document.forms.ymm.ymm_submodel.disabled = true;
        document.forms.ymm.ymm_engine.disabled = true
    }
}
function load_ymm_models(b, c, d) {
    document.forms.ymm.ymm_model.options.length = 1;
    document.forms.ymm.ymm_submodel.options.length = 1;
    document.forms.ymm.ymm_engine.options.length = 1;
    document.forms.ymm.ymm_model.disabled = true;
    document.forms.ymm.ymm_submodel.disabled = true;
    document.forms.ymm.ymm_engine.disabled = true;
    select_model_text = document.forms.ymm.ymm_model.options[0].text;
    if (c != "") {
        document.forms.ymm.ymm_model.options[0].text = "Loading models...";
        $.get("/partfinder/model/year/" + b + "/make/" + encodeURIComponent(encodeWithCase(c)) + "/0", {}, function (a) {
            a = $.trim(a);
            isEmpty = ('' == a);
            a = a.split("|");
            document.forms.ymm.ymm_model.options[0].text = select_model_text;
            if (!isEmpty && 1 == a.length) {
                document.forms.ymm.ymm_model.options[1] = new Option(a[0], encodeURIComponent(encodeWithCase(a[0])));
                document.forms.ymm.ymm_model.selectedIndex = 1;
                document.forms.ymm.ymm_model.disabled = false;
                modelChange(a[0]);
                if (typeof d == 'undefined' || (typeof d != 'undefined' && '' == d)) {
                    load_ymm_submodels(b, c, a[0])
                }
            } else if (!isEmpty) {
                for (i = 0; i < a.length; i++) {
                    document.forms.ymm.ymm_model.options[i + 1] = new Option(a[i], encodeURIComponent(encodeWithCase(a[i])));
                    if (d != "" && d != undefined) {
                        if (a[i].toLowerCase() == d.toLowerCase()) {
                            document.forms.ymm.ymm_model.selectedIndex = i + 1;
                            document.forms.ymm.ymm_model.disabled = false;
                            modelChange(d)
                        }
                    }
                }
                document.forms.ymm.ymm_model.disabled = false
            } else {
                modelChange(d)
            }
        })
    } else {
        document.forms.ymm.ymm_make.disabled = false;
        document.forms.ymm.ymm_model.disabled = true;
        document.forms.ymm.ymm_submodel.disabled = true;
        document.forms.ymm.ymm_engine.disabled = true
    }
}
function load_ymm_submodels(d, e, f, g, h) {
    var j = document.getElementById("nosubmit");
    var k = document.getElementById("ymm_model");
    document.forms.ymm.ymm_submodel.options.length = 1;
    document.forms.ymm.ymm_engine.options.length = 1;
    document.forms.ymm.ymm_submodel.disabled = true;
    document.forms.ymm.ymm_engine.disabled = true;
    select_submodel_text = document.forms.ymm.ymm_submodel.options[0].text;
    if (f != "") {
        document.forms.ymm.ymm_submodel.options[0].text = "Loading submodels...";
        $.ajax({type: "GET", url: "/partfinder/submodel/year/" + d + "/make/" + encodeURIComponent(encodeWithCase(e)) + "/model/" + encodeURIComponent(encodeWithCase(f)) + "/0", success: function (a) {
            a = $.trim(a);
            isEmpty = ('' == a);
            a = a.split("|");
            document.forms.ymm.ymm_submodel.options[0].text = select_submodel_text;
            if (!isEmpty && 1 == a.length) {
                document.forms.ymm.ymm_submodel.options[1] = new Option(a[0], encodeURIComponent(encodeWithCase(a[0])));
                document.forms.ymm.ymm_submodel.selectedIndex = 1;
                document.forms.ymm.ymm_submodel.disabled = false;
                submodelChange(a[0]);
                if (typeof h !== "undefined") {
                    h()
                }
                if (typeof g == 'undefined' || (typeof g != 'undefined' && '' == g)) {
                    load_ymm_engines(d, e, f, a[0])
                }
            } else if (!isEmpty) {
                for (i = 0; i < a.length; i++) {
                    document.forms.ymm.ymm_submodel.options[i + 1] = new Option(a[i], encodeURIComponent(encodeWithCase(a[i])));
                    if (g != "" && g != undefined) {
                        if (a[i].toLowerCase() == g.toLowerCase()) {
                            document.forms.ymm.ymm_submodel.selectedIndex = i + 1;
                            document.forms.ymm.ymm_submodel.disabled = false
                        }
                    }
                }
                submodelChange(g);
                if (typeof h !== "undefined") {
                    h()
                }
                document.forms.ymm.ymm_submodel.disabled = false
            } else if (isEmpty) {
                if (j == null) {
                    if (onChangeEvent) {
                        save_vehicle(d, e, f, '', '')
                    }
                }
            }
        }, error: function (a, b, c) {
            if (USAPCM.getCookie('vehicle_selected') == null || USAPCM.getCookie('user_save_vehicles') == null) {
                if (a.status == 404 || c == 'Not Found') {
                    save_vehicle(d, e, f, '', '')
                }
            }
        }})
    } else {
        document.forms.ymm.ymm_model.disabled = false;
        document.forms.ymm.ymm_submodel.disabled = true;
        document.forms.ymm.ymm_engine.disabled = true
    }
}
function load_ymm_engines(d, e, f, g, h, j) {
    var k = document.getElementById("nosubmit");
    var l = document.getElementById("ymm_submodel");
    document.forms.ymm.ymm_engine.options.length = 1;
    document.forms.ymm.ymm_engine.disabled = true;
    select_engine_text = document.forms.ymm.ymm_engine.options[0].text;
    if (g != "") {
        document.forms.ymm.ymm_engine.options[0].text = "Loading engines...";
        $.ajax({type: "GET", url: "/partfinder/engine/year/" + d + "/make/" + encodeURIComponent(encodeWithCase(e)) + "/model/" + encodeURIComponent(encodeWithCase(f)) + "/submodel/" + encodeURIComponent(encodeWithCase(g)) + "/0", success: function (a) {
            a = $.trim(a);
            isEmpty = ('' == a);
            a = a.split("|");
            document.forms.ymm.ymm_engine.options[0].text = select_engine_text;
            if (!isEmpty && 1 == a.length) {
                document.forms.ymm.ymm_engine.options[1] = new Option(a[0], encodeURIComponent(encodeWithCase(a[0])));
                document.forms.ymm.ymm_engine.selectedIndex = 1;
                document.forms.ymm.ymm_engine.disabled = true;
                if (k != null) {
                    document.forms.ymm.ymm_engine.disabled = false
                }
                engineChange(h);
                if (typeof j !== "undefined") {
                    j()
                }
                if (typeof h == 'undefined' || (typeof h != 'undefined' && '' == h)) {
                    if (k == null) {
                        if (onChangeEvent) {
                            save_vehicle(d, e, f, g, a[0])
                        }
                    }
                }
            } else if (!isEmpty) {
                for (i = 0; i < a.length; i++) {
                    document.forms.ymm.ymm_engine.options[i + 1] = new Option(a[i], encodeURIComponent(encodeWithCase(a[i])));
                    if (h != "" && h != undefined) {
                        if (a[i].toLowerCase() == h.toLowerCase()) {
                            document.forms.ymm.ymm_engine.selectedIndex = i + 1;
                            document.forms.ymm.ymm_engine.disabled = false
                        }
                    }
                }
                engineChange(h);
                if (typeof j !== "undefined") {
                    j()
                }
                document.forms.ymm.ymm_engine.disabled = false
            } else if (isEmpty) {
                document.forms.ymm.ymm_engine.disabled = true;
                if (k == null) {
                    if (onChangeEvent) {
                        save_vehicle(d, e, f, g, '')
                    }
                }
            }
        }, error: function (a, b, c) {
            if (USAPCM.getCookie('vehicle_selected') == null || USAPCM.getCookie('user_save_vehicles') == null) {
                if (a.status == 404 || c == 'Not Found') {
                    save_vehicle(d, e, f, g, '')
                }
            }
        }})
    } else {
        document.forms.ymm.ymm_submodel.disabled = false;
        document.forms.ymm.ymm_engine.disabled = true
    }
}
function check_ymm_model(a, b, c, d, e) {
    if (a == '') {
        alert('Please select a year')
    } else if (b == '') {
        alert('Please select a make')
    } else if (c == '') {
        alert('Please select a model')
    } else {
        save_vehicle(a, b, c, d, e)
    }
}
function partfinder_init() {
    $('.select-2 select, .select-3 select, .select-4 select, .select-5 select').attr('disabled', 'true');
    $('#shop-by-vehicle select').change(function () {
        $('#shop-by-vehicle .highlight').removeClass('highlight')
    })
}
function yearChange(a) {
    for (var i = 1; i <= 5; ++i) {
        $('.select-' + i).removeClass('highlight')
    }
    if (a != "") {
        $('.select-2').addClass('highlight');
        if ($('.select-2 select').length)$('.select-2 select').removeAttr('disabled')[0].focus();
        for (var i = 3; i <= 5; ++i) {
            $('.select-' + i + ' select').attr('disabled', 'true')
        }
    } else {
        for (var i = 2; i <= 5; ++i) {
            $('.select-' + i + ' select').attr('disabled', 'true')
        }
        $('.select-1').addClass('highlight')
    }
    $('.select-4, .select-5').slideUp('fast')
}
function makeChange(a) {
    for (var i = 1; i <= 5; ++i) {
        $('.select-' + i).removeClass('highlight')
    }
    if (a != "") {
        $('.select-3').addClass('highlight');
        if ($('.select-3 select').length)$('.select-3 select').removeAttr('disabled')[0].focus();
        for (var i = 4; i <= 5; ++i) {
            $('.select-' + i + ' select').attr('disabled', 'true')
        }
    } else {
        $('.select-2').addClass('highlight')
    }
    $('.select-4, .select-5').slideUp('fast')
}
function modelChange(a) {
    for (var i = 1; i <= 5; ++i) {
        $('.select-' + i).removeClass('highlight')
    }
    if (a == '') {
        $('.select-4, .select-5').slideUp('fast');
        $('.select-3').addClass('highlight')
    }
}
function submodelChange(a) {
    for (var i = 1; i <= 5; ++i) {
        $('.select-' + i).removeClass('highlight')
    }
    if (a != "" && a != undefined) {
        if ($('.select-4').css('display') == 'none') {
            $('.select-4').animate({'height': 'toggle', 'opacity': 'toggle'}, 'fast')
        }
        if ($('.select-5').css('display') == 'none') {
            $('.select-5').animate({'height': 'toggle', 'opacity': 'toggle'}, 'fast')
        }
        if ($('.select-5 select').length)$('.select-5 select').removeAttr('disabled');
        $('.select-5').addClass('highlight');
        if (ymmSavedVehicleLastFocus !== "engine") {
            ymmSavedVehicleLastFocus = null;
            $('#ymm_engine').focus()
        }
    } else {
        if ($('.select-4').css('display') == 'none') {
            $('.select-4').animate({'height': 'toggle', 'opacity': 'toggle'}, 'fast')
        }
        if ($('.select-4 select').length)$('.select-4 select').removeAttr('disabled');
        $('.select-4').addClass('highlight');
        if (ymmSavedVehicleLastFocus !== "submodel") {
            ymmSavedVehicleLastFocus = null;
            $('#ymm_submodel').focus()
        }
        $('.select-5 select').attr('disabled', 'true');
        $('.select-5').slideUp('fast')
    }
}
function engineChange(a) {
    for (var i = 1; i <= 5; ++i) {
        $('.select-' + i).removeClass('highlight')
    }
    if ($('.select-5').css('display') == 'none') {
        $('.select-5').animate({'height': 'toggle', 'opacity': 'toggle'}, 'fast')
    }
    $('.select-5').addClass('highlight')
}
function ManageVehicleFits() {
    var e = document.getElementById("ymm_year").value;
    var f = document.getElementById("ymm_make").value;
    var g = document.getElementById("ymm_model").value;
    var h = document.getElementById("ymm_submodel").value;
    var i = document.getElementById("ymm_engine").value;
    var j = document.getElementById("brand").value;
    var k = document.getElementById("sku").value;
    var l = document.getElementById("part").value;
    var m = document.getElementById("tlc").value;
    setVehicleSelectionPageRef();
    var n = "/vehiclefits/" + e + "/" + f + "/" + g + "/";
    var o = "year:" + e + ",make:" + escapeQueryValue(encodeNrValue(decode(f))) + ",model:" + escapeQueryValue(encodeNrValue(decode(g)));
    if (h != '') {
        n = n + h + "/";
        o = o + ",submodel:" + escapeQueryValue(encodeNrValue(decode(h)))
    }
    if (i != '') {
        n = n + i + "/";
        o = o + ",engine:" + escapeQueryValue(encodeNrValue(decode(i)))
    }
    n = n + l + "/" + k + "/" + j + "/";
    $.get(n, {}, function (b) {
        if (b == 0) {
            document.getElementById("overlay-year").innerHTML = decode(e);
            document.getElementById("overlay-make").innerHTML = decode(f);
            document.getElementById("overlay-model").innerHTML = decode(g);
            document.getElementById("overlay-submodel").innerHTML = decode(h);
            document.getElementById("overlay-engine").innerHTML = decode(i);
            param_brand_partname_prods = 'N=0&Nr=AND(' + o + ',part:' + escapeQueryValue(encodeNrValue(decode(l))) + ',brand:' + escapeQueryValue(encodeNrValue(decode(j))) + ')';
            url_brand_partname_prods = '/search/?N=0&Nr=AND(' + o + ',part:' + escapeQueryValue(encodeNrValue(decode(l))) + ',brand:' + escapeQueryValue(encodeNrValue(decode(j))) + ')';
            param_partname_prods = 'N=0&Nr=AND(' + o + ',part:' + escapeQueryValue(encodeNrValue(decode(l))) + ')';
            url_partname_prods = '/search/?N=0&Nr=AND(' + o + ',part:' + escapeQueryValue(encodeNrValue(decode(l))) + ')';
            url_all_parts_prods = '/vehicle/' + e + '/' + f + '/' + g;
            if (h != '')url_all_parts_prods = url_all_parts_prods + '/' + h;
            if (i != '')url_all_parts_prods = url_all_parts_prods + '/' + i;
            url_all_parts_prods = url_all_parts_prods + '.html';
            document.getElementById("link_brand_partname").setAttribute('href', url_brand_partname_prods);
            document.getElementById("link_partname").setAttribute('href', url_partname_prods);
            document.getElementById("link_all_parts").setAttribute('href', url_all_parts_prods);
            if (l.substr(-1) != 's') {
                document.getElementById("link_brand_partname").innerHTML = j + ' ' + l + 's';
                document.getElementById("link_partname").innerHTML = l + 's'
            } else {
                document.getElementById("link_brand_partname").innerHTML = j + ' ' + l;
                document.getElementById("link_partname").innerHTML = l + 's'
            }
            document.getElementById("li_brand_partname").style.display = 'none';
            document.getElementById("li_partname").style.display = 'none';
            document.getElementById("li_all_parts").style.display = 'none';
            $.get("/prodcount/?" + param_brand_partname_prods, {}, function (a) {
                if (a == '1') {
                    document.getElementById("li_brand_partname").style.display = 'block'
                } else {
                    document.getElementById("li_all_parts").style.display = 'block'
                }
            });
            $.get("/prodcount/?" + param_partname_prods, {}, function (a) {
                if (a == 1) {
                    document.getElementById("li_partname").style.display = 'block'
                } else {
                    document.getElementById("li_all_parts").style.display = 'block'
                }
            });
            disablePopup();
            target_id = "#overlay-no-fit";
            centerPopup(target_id);
            loadPopup(target_id);
            return false
        } else {
            if (m != '') {
                var c = "/sku/" + escape(f) + "/" + escape(g) + "/" + escape(j) + "/" + escape(l) + "/" + e + "/";
                if (h != '')c = c + escape(h) + "/";
                if (i != '')c = c + escape(i) + "/";
                c = c + escape(k) + ".html";
                if (m != null)c = c + "?tlc=" + escape(m);
                window.location = c
            } else {
                var d = "/details/QQ" + escape(f) + "QQ" + escape(g) + "QQ" + escape(j) + "QQ" + escape(l) + "QQ" + e + "QQ";
                if (h != '')d = d + escape(h) + "QQ";
                if (i != '')d = d + escape(i) + "QQ";
                d = d + escape(k) + ".html";
                window.location = d
            }
        }
    })
}
