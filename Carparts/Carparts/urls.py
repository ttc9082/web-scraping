from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Carparts.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^test/$', 'autopartswarehouse.views.get_years_and_make_and_model'),
    url(r'^test2/$', 'autopartswarehouse.views.get_all_category_urls'),
)
