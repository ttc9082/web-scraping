from django.db import models

# Create your models here.


class Brand(models.Model):
    name = models.CharField(max_length=500)

    def __unicode__(self):
        return self.name


class Model(models.Model):
    name = models.CharField(max_length=100)
    brand = models.ForeignKey(Brand)
    year = models.IntegerField()
    model_cookie = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class Part(models.Model):
    name = models.CharField(max_length=500)
    part = models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=100)
    sale = models.DecimalField(max_digits=8, decimal_places=2)
    model = models.ManyToManyField(Model)
    origin = models.DecimalField(max_digits=8, decimal_places=2)
    date = models.DateField()
    zip = models.IntegerField()

    def __unicode__(self):
        return self.name





