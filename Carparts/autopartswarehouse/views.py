import urllib2
import urllib
import time
import MySQLdb
import random
import socket
import struct
from bs4 import BeautifulSoup
import datetime
import calendar
import os.path
import threading
import cookielib
import json
from django.shortcuts import render
from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.views.decorators.csrf import csrf_protect
from django.core.context_processors import csrf
from .models import *
from django.utils import timezone
from django.contrib.auth.decorators import login_required
# Create your views here.


def browse(url, cookies=''):
    ip_address = socket.inet_ntoa(struct.pack('>I', random.randint(1, 0xffffffff)))
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6',
        'X-Forwarded-For': ip_address,
        'Cookie': cookies
    }
    my_url = url
    my_request = urllib2.Request(my_url, headers=headers)
    my_response = urllib2.urlopen(my_request, timeout=10)
    # print my_response.geturl()
    real_url = my_response.geturl()
    my_page = my_response.read()
    return my_page, real_url


def get_years_and_make_and_model(request):
    year_list = []
    for i in xrange(9):
        year_list.append(str(2007+i))
    print year_list
    for year in year_list:
        find_brand_url = 'http://www.autopartswarehouse.com/partfinder/make/year/' + year
        temp = 0
        while True:
            try:
                brand_list = BeautifulSoup(browse(find_brand_url)[0]).find('body').text.split('|')
                break
            except:
                temp += 1
                if temp > 3:
                    break
        print brand_list
        for brand in brand_list:
            new_brand, created = Brand.objects.get_or_create(name=brand)
            print created
            find_model_url = 'http://www.autopartswarehouse.com/partfinder/model/year/' + year \
                             + '/make/' + brand.replace(' ', '%20')
            print find_model_url
            while True:
                try:
                    model_list = BeautifulSoup(browse(find_model_url)[0]).find('body').text.split('|')
                    for model in model_list:
                        cj = cookielib.CookieJar()
                        opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
                        urllib2.install_opener(opener)
                        my_request = urllib2.Request('http://www.autopartswarehouse.com/managevehicle/'
                                                     '?status=add&year=' + year
                                                     + '&make=' + brand.lower().replace(' ', '%20')
                                                     + '&model=' + model.lower() + '&submodel=&engine=')
                        my_response = urllib2.urlopen(my_request)
                        print cj
                        cookie = ''
                        for c in cj:
                            cookie += c.name + '=' + c.value + ';'
                            if c.name == 'user_save_vehicles':
                                model_cookie = c.value
                        cj.clear()
                        this_model, created = Model.objects.get_or_create(name=model, brand=new_brand, year=year, model_cookie=model_cookie)
                        print this_model, created
                    break
                except:
                    temp += 1
                    if temp > 3:
                        break
    return HttpResponse('hahaha')


def get_cookies(request):
    cj = cookielib.CookieJar()
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
    urllib2.install_opener(opener)
    print 'hahaha', cj._cookies.values()
    my_request = urllib2.Request('http://www.autopartswarehouse.com/managevehicle/?status=add&year=2014&make=bmw&model=328i&submodel=&engine=')
    my_response = urllib2.urlopen(my_request)
    print cj
    cookie = ''
    for c in cj:
        cookie += c.name + '=' + c.value + ';'
    cj.clear()
    response = opener.open('http://www.autopartswarehouse.com/search/?N=0&Nr=AND(wpn_tl_name:Auto+Body+Parts+%26+Mirrors,wpn_cat_name:Bumpers+%26+Components,wpn_scat_name:Bumper+Cover)')
    page = response.read()
    print cookie
    # cookie = 'LastInvite=Mon%2C%2005%20May%202014%2022%3A01%3A09%20GMT; LastVisit=Thu%2C%2008%20May%202014%2018%3A33%3A28%20GMT; C_RICD3=1; userID=694ec4b776c5b5c05869b236fee6fb0e; __gads=ID=d2f5a0be34246d0c:T=1399325588:S=ALNI_MYMdKsYpHzk9Lb2vdOkDt81cEpKMw; visitor_uid=0.9052220203448087; RES_TRACKINGID=21752337309552399; ClrOSSID=1399325591265-11731; ClrSCD=1399325591265; mvt_all_traffic=b%7C20; mvt_skucertona=a%7C50; __cfduid=df1ef9a0adfe564b433a6963f266e79f41399559168374; s_eVar5=Product%2BListings%2BBest%2BSeller; WebIQFirstPartyCookieCheck=1; WebIQOptInAttempted-{7E41BD6E-6D5A-4403-9AC4-13426A76F920}=1; WebIQActiveProjectVersion={00000000-0000-0000-0000-000000000000}; WebIQActiveSession={00000000-0000-0000-0000-000000000000}; WebIQOptInResponseTime-{7E41BD6E-6D5A-4403-9AC4-13426A76F920}=5/8/2014 19:01:19; WebIQOptInResponseType-{7E41BD6E-6D5A-4403-9AC4-13426A76F920}=4; LastVisit=Thu%2C%2008%20May%202014%2019%3A06%3A31%20GMT; mbox=session#1399573908567-850231#1399577886|check#true#1399576086; AKSB=s=1399576102868&r=http%3A//www.autopartswarehouse.com/search/%3FN%3D0%26Nr%3DAND%2528wpn_tl_name%253AAuto+Body+Parts+%2526+Mirrors%252Cwpn_cat_name%253ABumpers+%2526+Components%252Cwpn_scat_name%253AStep+Bumpers+%2526+Components%252Cpart%253ABumper+Step+Pad%252Cuniversal%253A0%2529%26use_warranty_code_parent%3DAutoTrust%26mvtgroup%3DB%26Npp%3D50; search_session_id=536bd870173ce7.61907763; apw30days=%5B%5D; user_save_vehicles=115-74-1807-1295; vehicle_selected=115-74-1807-1295; apwsession={"userSessionId":["c8897e0fab9f16fe856aabe5df9ef671",0]}; s_cc=true; provars=%7B%22c%22%3A%7B%22SearchResultPageVisitCount%22%3A17%2C%22SrchNum%22%3A20%2C%22ProdPageVisitCount%22%3A2%2C%22NoSearchResultPageVisitCount%22%3A2%2C%22NullResult%22%3A1%7D%2C%22s%22%3A%7B%7D%2C%22e%22%3A%7B%22NumberOfFormErrors%22%3Anull%7D%7D; RES_SESSIONID=29639174920937599; ResonanceSegment=1; ver=b; mvt=b; mvt_split=70; site_split=100; mobiledevice=non-mobile; __utma=10727844.681768951.1399325591.1399559172.1399573903.3; __utmb=10727844.82.9.1399576697028; __utmc=10727844; __utmz=10727844.1399325591.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); utag_main=_st:1399578497078$ses_id:1399574868563%3Bexp-session; s_vi=[CS]v1|29B401CA851D03C5-40000102A0398134[CE]; WebIQ.API.Debug=false; ClrCSTO=T; ClrSSID=1399325591265-11731; WarpFirstPartyCookieCheck=1; WebIQWARPSessionID={ef33966b-008c-4a9d-ad81-2f239da892c1}; WebIQWARPReferrer=; usAutoPartsInviteCancel=1; anlytxpt=ComponentName%3DProduct%20Results%26ProductSlot%3D1; proptag=Product Title; usapgpv_p5=Auto%20Body%20Parts%20%26%20Mirrors%3A%20Bumpers%20%26%20Components%3A%20Step%20Bumpers%20%26%20Components; usapgpv_p4=Product%20Listings%20Page; usapgpv_p3=Product%20Listings%20Page; s_sq=usautopartswh%3D%2526pid%253DAuto%252520Body%252520Parts%252520%252526%252520Mirrors%25253A%252520Bumpers%252520%252526%252520Components%25253A%252520Step%252520Bumpers%252520%252526%252520Components%2526pidt%253D1%2526oid%253Dhttp%25253A%25252F%25252Fwww.autopartswarehouse.com%25252Fsku%25252FUniv%25252FPacer%25252FBumper_Step_Pad%25252FP6222292.html%25253Ftlc%25253DAuto%25252BBody%25252BParts%25252B%252525%2526ot%253DA'
    # page = urllib2.urlopen(urllib2.Request('http://www.autopartswarehouse.com/search/?N=0&Nr=AND(wpn_tl_name:Auto+Body+Parts+%26+Mirrors,wpn_cat_name:Bumpers+%26+Components,wpn_scat_name:Step+Bumpers+%26+Components,part:Bumper+Step+Pad)')).read()
    # req = urllib2.Request('http://www.autopartswarehouse.com/vehicle/')
    # req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6')
    # req.add_header('Referer', 'http://www.autopartswarehouse.com/')
    # data = {'year': '2014', 'make': 'BMW', 'model': '335i', 'page_ref': 'Vehicle Landing Page'}
    # data = urllib.urlencode(data)
    # opener = urllib2.build_opener(urllib2.HTTPCookieProcessor())
    # response = opener.open(req, data)
    # check content type:
    # ct = response.info().getheader('Set-Cookie')
    return HttpResponse(page)


def get_all_category_urls(request):
    all_urls = []
    index = 'http://www.autopartswarehouse.com/'
    main_page = browse(index)[0]
    soup = BeautifulSoup(main_page)
    cates = soup.find_all('li', {'class': 'cat-see-all'})
    for cate in cates:
        all_urls.append(cate.a['href'])
    all_urls.remove(all_urls[-1])
    all_urls.append('http://www.autopartswarehouse.com/category/wheels.html')
    main_urls = ['http://www.autopartswarehouse.com/category/auto_body_parts_-and-_mirrors.html',
                'http://www.autopartswarehouse.com/category/headlights_-and-_lighting.html',
                'http://www.autopartswarehouse.com/category/engine_-and-_drivetrain.html',
                'http://www.autopartswarehouse.com/category/brakes-comma-_suspension_-and-_steering.html',
                'http://www.autopartswarehouse.com/category/interior_accessories.html',
                'http://www.autopartswarehouse.com/category/exterior_accessories.html',
                'http://www.autopartswarehouse.com/category/wheels.html']
    main_cate_urls = []
    for url in main_urls:
        page = browse(url)[0]
        soup = BeautifulSoup(page)
        product_name = soup.find('div', {'id': 'all-cat'}).find_all('a', {'class': 'product-name'})
        for subcate in product_name:
            main_cate_urls.append(subcate['href'])
    sub_cate_urls = []
    for url in main_cate_urls:
        page, real_url = browse(url)
        print url
        soup = BeautifulSoup(page)
        path_num = len(soup.find('div', {'class', 'path'}).find_all('a'))
        if path_num > 2:
            sub_cate_urls.append(real_url)
            print 'yeah!'
        else:
            product_name = soup.find('div', {'id': 'all-cat'}).find_all('a', {'class': 'product-name'})
            for subcate in product_name:
                sub_cate_urls.append(subcate['href'])
    print len(sub_cate_urls)
    log(str(sub_cate_urls))
    return HttpResponse(str(sub_cate_urls))

def log(log_content):
    f = open('log/log.json', 'w')
    f.write(log_content)
    f.close()
    return True